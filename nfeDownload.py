from selenium import webdriver # type: ignore
from selenium.webdriver.common.by import By # type: ignore
from selenium.webdriver.common.keys import Keys # type: ignore
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait # type: ignore
import re
import time
import os
import json
from copyFolder import copyFolder
from uploadFile import UploadFileNfe
#FIREFOX
# from selenium.webdriver.firefox.service import Service
# from selenium.webdriver.firefox.options import Options
# from webdriver_manager.firefox import GeckoDriverManager
##CHROME
#from selenium.webdriver.chrome.service import Service 
#from selenium.webdriver.chrome.options import Options 
#from webdriver_manager.chrome import ChromeDriverManager
##EDGE
from selenium.webdriver.edge.service import Service  # type: ignore
from selenium.webdriver.edge.options import Options  # type: ignore
from webdriver_manager.microsoft import EdgeChromiumDriverManager # type: ignore

base_directory = os.path.dirname(os.path.abspath(__file__))
if not os.path.exists('nfe'):
    os.makedirs('nfe')
 
download_directory = os.path.join(base_directory, "nfe")

if not os.path.exists('zip'):
    os.makedirs('zip')   


# options = Options()
# options.headless = True
# options.set_preference("browser.download.folderList", 2)
# options.set_preference("browser.download.manager.showWhenStarting", False)
# options.set_preference("browser.download.dir", download_directory)
# options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
# options.set_preference("pdfjs.disabled", True) 

options = Options() 
options.headless = True  

prefs = {
    "download.default_directory": download_directory,
    "download.prompt_for_download": False,
    "download.directory_upgrade": True,
    "safebrowsing.enabled": False,
}
options.add_experimental_option("prefs", prefs)

#service = Service(GeckoDriverManager().install())
service = Service(EdgeChromiumDriverManager().install())

def clearFolder(folder_path):
    # Obtém a lista de arquivos no diretório
    files = os.listdir(folder_path)

    # Remove cada arquivo individualmente
    for file in files:
        file_path = os.path.join(folder_path, file)
        if os.path.isfile(file_path):
            os.remove(file_path)

    print(f"Conteúdo removido da pasta {folder_path} com sucesso!")
    
TIMEOUT = 60 

def aguardar_download_concluir(download_dir, timeout=TIMEOUT):
    """
    Aguarda até que o download esteja concluído.
    Verifica a ausência de arquivos temporários no diretório.
    """
    tempo_inicial = time.time()
    while True:
        arquivos = os.listdir(download_dir)
        if not any(arquivo.endswith(".crdownload") or arquivo.endswith(".tmp") for arquivo in arquivos):
            break
        if time.time() - tempo_inicial > timeout:
            raise TimeoutError("O download não foi concluído dentro do tempo limite.")
        time.sleep(1)
        
def crawlerDownload(
        id: str,
        userNfeReceitaPr: str,
        passwordNfeReceitaPr: str,
        documentNumber: str, 
        documentNumberOffice: str,        
        escritorioId: str,
        empresaId: str,
        razaoSocial: str,
        requestNumber_destinadas: str,
        requestNumber_emitidas: str,
        referenceDate: str,
) -> dict:
    cnpjFormatado = re.sub(r'^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$', r'\1.\2.\3/\4-\5', documentNumber)
    url = 'https://www.dfeportal.fazenda.pr.gov.br/dfe-portal/manterDownloadDFe.do?action=iniciar'
    url_destinadas = ''
    url_emitidas = ''
    docType = ''
    countInvoices_destinadas = ''
    description_destinadas = ''
    countInvoices_emitidas = ''
    description_emitidas = ''
    processDate = ''
    requestDate = ''
    periodo = '' 
    status_nfe = ''  
    
    clearFolder('nfe') 
    clearFolder('zip')    

    #page = webdriver.Firefox(service=service, options=options)  
    page = webdriver.Edge(service=service, options=options)
     
    page.maximize_window()
    page.get(url)    
    
    time.sleep(5)
    page.find_element(By.XPATH, '//*[@id="cpfusuario"]').send_keys(userNfeReceitaPr)
    page.find_element(By.XPATH, '/html/body/div[2]/form[1]/div[3]/div/input').send_keys(passwordNfeReceitaPr)
    page.find_element(By.XPATH,'/html/body/div[2]/form[1]/div[4]/button').click()

    time.sleep(2)
    page.find_element(By.XPATH, '//*[@id="ext-gen1081"]').send_keys(cnpjFormatado)
    time.sleep(2)
    page.find_element(By.XPATH, '//*[@id="ext-gen1081"]').send_keys(Keys.TAB)
    #page.find_element(By.ID, 'ucs20_ToolBarbtnAtualizar-btnInnerEl').click()
    time.sleep(5)

    tabela_selector = "table.x-grid-table.x-grid-table-resizer"
    linhas_selector = f"{tabela_selector} tbody tr:not(:first-child)"
    linhas = page.find_elements(By.CSS_SELECTOR, linhas_selector)

    resultados = []




    wait = WebDriverWait(page, TIMEOUT)

    for linha in linhas:
        celulas_selector = "td"
        celulas = linha.find_elements(By.CSS_SELECTOR, celulas_selector)
        target_imgs = linha.find_elements(By.CSS_SELECTOR,
                                          "img.x-action-col-icon.x-action-col-1.x-grid-center-icon")
        if target_imgs:
            target_img = target_imgs[0]
            actions = ActionChains(page)
            actions.move_to_element(target_img).click().perform()

        objeto_linha = {}

        time.sleep(1)

        for i, celula in enumerate(celulas, start=1):
            texto_celula = celula.text.strip()
            objeto_linha[f"coluna{i}"] = texto_celula

        resultados.append(objeto_linha)

        if objeto_linha["coluna1"] == requestNumber_emitidas:

            if objeto_linha["coluna10"] == "Solicitação processada. Clique na imagem para realizar o download.":
                docType = objeto_linha["coluna4"]
                
                
                matches = re.findall(r'\d{2}/\d{2}/\d{4}', objeto_linha["coluna3"])
                date1 = matches[0].replace('/', '-')
                date2 = matches[1].replace('/', '-')
                period = f"{date1}_a_{date2}"
                time.sleep(35)
                newFolderName = f"DowloadsNfes-{documentNumber}.zip";
                fileName = newFolderName
                time.sleep(5)
                copyFolder(newFolderName, requestNumber_emitidas)

                time.sleep(2)
                referenceDateStorage = referenceDate.replace('/', '-')
                
                docType = "Emitidas"
                url_emitidas =  f"https://storage.cloud.google.com/othree-notas/NFe/PR/{documentNumberOffice}/{documentNumber}/{referenceDateStorage}/{period}/{docType}/{fileName}"

                time.sleep(2)
                upload_file_instance = UploadFileNfe.get_instance()
                upload_file_instance.execute(      
                    
                    documentNumberOffice,
                    documentNumber,
                    referenceDateStorage,                    
                    period, 
                    docType,                 
                    fileName
                )
                countInvoices_emitidas = objeto_linha["coluna11"]
                description_emitidas = objeto_linha["coluna10"].split('.')[0].strip()
                processDate = objeto_linha["coluna9"]
                requestDate = objeto_linha["coluna8"]
                periodo = objeto_linha["coluna3"]                                
               
            elif objeto_linha["coluna10"] == "Solicitação expirada.":
                
                countInvoices_emitidas = objeto_linha["coluna11"]
                description_emitidas = objeto_linha["coluna10"].split('.')[0].strip() 
                processDate = objeto_linha["coluna9"]
                requestDate = objeto_linha["coluna8"]
                periodo = objeto_linha["coluna3"]                             
                url_emitidas = "TBD"              
                
            elif objeto_linha["coluna10"] == "Não foram encontrados documentos para esta solicitação.":
                countInvoices_emitidas = objeto_linha["coluna11"]
                description_emitidas = objeto_linha["coluna10"].split('.')[0].strip()
                processDate = objeto_linha["coluna9"]
                requestDate = objeto_linha["coluna8"]
                periodo = objeto_linha["coluna3"]                              
                url_emitidas = "TBD"
                
            elif objeto_linha["coluna10"] == "Solictação cancelada.":
                countInvoices_emitidas = objeto_linha["coluna11"]
                description_emitidas = objeto_linha["coluna10"].split('.')[0].strip()
                processDate = objeto_linha["coluna9"]
                requestDate = objeto_linha["coluna8"]
                periodo = objeto_linha["coluna3"]                              
                url_emitidas = "TBD"
            else:
                resultado_encontrado = {
                  "Mensagem": 'Solicitação já foi agendada e está em análise',
                }
                page.quit()
                return resultado_encontrado
            
        if objeto_linha["coluna1"] == requestNumber_destinadas:

            if objeto_linha["coluna10"] == "Solicitação processada. Clique na imagem para realizar o download.":
                docType = objeto_linha["coluna4"]
                
                status_nfe = "PLANNED" 
                matches = re.findall(r'\d{2}/\d{2}/\d{4}', objeto_linha["coluna3"])
                date1 = matches[0].replace('/', '-')
                date2 = matches[1].replace('/', '-')
                period = f"{date1}_a_{date2}" 
                time.sleep(35)               
                newFolderName = f"DowloadsNfes-{documentNumber}.zip";
                fileName = newFolderName
                time.sleep(5)
                copyFolder(newFolderName, requestNumber_destinadas)
                time.sleep(2)
                referenceDateStorage = referenceDate.replace('/', '-')
                
                docType = "Destinadas"
                url_destinadas =  f"https://storage.cloud.google.com/othree-notas/NFe/PR/{documentNumberOffice}/{documentNumber}/{referenceDateStorage}/{period}/{docType}/{fileName}"

                time.sleep(2)
                upload_file_instance = UploadFileNfe.get_instance()
                upload_file_instance.execute(  
                    documentNumberOffice,                  
                    documentNumber,                    
                    referenceDateStorage,                    
                    period, 
                    docType,                 
                    fileName
                )
                countInvoices_destinadas = objeto_linha["coluna11"]
                description_destinadas = objeto_linha["coluna10"].split('.')[0].strip()                                
                
            elif objeto_linha["coluna10"] == "Solicitação expirada.":
                
                countInvoices_destinadas = objeto_linha["coluna11"]
                description_destinadas = objeto_linha["coluna10"].split('.')[0].strip()               
                url_destinadas = "TBD"
                status_nfe = "FALHA"              
                
            elif objeto_linha["coluna10"] == "Não foram encontrados documentos para esta solicitação.":
                countInvoices_destinadas = objeto_linha["coluna11"]
                description_destinadas = objeto_linha["coluna10"].split('.')[0].strip()               
                url_destinadas = "TBD"
                status_nfe = "PLANNED"
                
            elif objeto_linha["coluna10"] == "Solictação cancelada.":
                countInvoices_destinadas = objeto_linha["coluna11"]
                description_destinadas = objeto_linha["coluna10"].split('.')[0].strip()               
                url_destinadas = "TBD"
                status_nfe = "FALHA"
                
                       

    resultado_encontrado = {
        "id": id,
        "filterDate": periodo,
        "countInvoices_destinadas": countInvoices_destinadas,
        "countInvoices_emitidas": countInvoices_emitidas,
        "documentNumber": documentNumber, 
        "documentNumberOffice": documentNumberOffice,              
        "description_emitidas":  description_emitidas,
        "description_destinadas": description_destinadas,
        "escritorioId": escritorioId,
        "empresaId": empresaId,
        "requestNumber_destinadas": requestNumber_destinadas,
        "requestNumber_emitidas": requestNumber_emitidas,        
        "processDate": processDate,
        "requestDate": requestDate,
        "referenceDate": referenceDate,
        "razaoSocial": razaoSocial,
        "urlFile_emitidas": url_emitidas,
        "urlFile_destinadas": url_destinadas,
        "msg": f"Downloads Cnpj: {documentNumber}, Realizado Com Sucesso",
        "status": status_nfe
    }

    print("Resultado encontrado:")
    print(json.dumps(resultado_encontrado, indent=2, ensure_ascii=False).encode('utf-8').decode('utf-8'))                
    page.quit()
    return resultado_encontrado
    
            
    page.quit()
