from selenium import webdriver 
from selenium.webdriver.common.by import By 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.common.action_chains import ActionChains 
from selenium.common.exceptions import NoSuchElementException
from datetime import datetime
import json
import time
import re
import os

#FIREFOX
# from selenium.webdriver.firefox.service import Service
# from selenium.webdriver.firefox.options import Options
# from webdriver_manager.firefox import GeckoDriverManager

##CHROME
# from selenium.webdriver.chrome.service import Service 
# from selenium.webdriver.chrome.options import Options 
# from webdriver_manager.chrome import ChromeDriverManager

##EDGE
from selenium.webdriver.edge.service import Service  
from selenium.webdriver.edge.options import Options  
from webdriver_manager.microsoft import EdgeChromiumDriverManager 

options = Options()
options.headless = False

#service = Service(GeckoDriverManager().install())
service = Service(EdgeChromiumDriverManager().install())

def crawlerAgendar(
    id:str,
    userNfeReceitaPr: str,
    passwordNfeReceitaPr: str,
    documentNumber: str,
    documentNumberOffice: str,        
    initialDate: str,
    endDate: str,
    escritorioId: str,
    empresaId: str,
    razaoSocial: str
) -> dict:

    initialHour = '000000'
    endHour = '235959'
    cnpjFormatado = re.sub(r'^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$', r'\1.\2.\3/\4-\5', documentNumber)
    initialDateFormatted = re.sub(r'(\d{2})(\d{2})(\d{4})', r'\1/\2/\3', initialDate)
    endDateFormatted = re.sub(r'(\d{2})(\d{2})(\d{4})', r'\1/\2/\3', endDate)
    url = 'https://www.dfeportal.fazenda.pr.gov.br/dfe-portal/manterDownloadDFe.do?action=iniciar' 
    split_date = initialDate.split('/')
    formatted_date = split_date[1] + '/' + split_date[2]  
    description_destinadas = ''
    request_number_destinadas = ''
    description_emitidas = ''
    request_number_emitidas = ''
    data_hora_atual = datetime.now()            
    formato = "%d/%m/%Y %H:%M:%S"
    atual_date = data_hora_atual.strftime(formato)
    filterDate = f"{initialDateFormatted} 00:00:00 a {endDateFormatted} 23:59:59"  
    periodo = ''
    data_processamento = ''
    data_requisicao = ''
    
    page = webdriver.Edge(service=service, options=options)
    #page = webdriver.Firefox(service=service, options=options)
    #page = webdriver.Remote(command_executor='http://62.72.63.185:4444', options=options)      
   
    page.maximize_window()
    page.get(url)

    time.sleep(5)  
    page.find_element(By.XPATH, '//*[@id="cpfusuario"]').send_keys(userNfeReceitaPr)
    page.find_element(By.XPATH, '/html/body/div[2]/form[1]/div[3]/div/input').send_keys(passwordNfeReceitaPr)
    page.find_element(By.XPATH,'/html/body/div[2]/form[1]/div[4]/button').click()

    time.sleep(3)  
    elemento = None 
    janela = None 
    
    page.find_element(By.CSS_SELECTOR, '#ext-gen1112').click()  
    
    time.sleep(2)
    
    page.find_element(By.CSS_SELECTOR, '#ext-gen1081').send_keys(cnpjFormatado)
    time.sleep(1)  
    page.find_element(By.CSS_SELECTOR, '#ext-gen1081').send_keys(Keys.TAB)
    
    janelas = page.window_handles 
    print(janelas)
    
    time.sleep(2)

    

    try:
        # Supondo que você já tenha o driver e a página carregada
        janela = page.find_element(By.XPATH, '//*[@id="header-1093"]')
        mensagem = janela.text.strip()
        
        # Verifica se a mensagem específica está presente
        if "Não foram encontrados registros para os parâmetros informados." in mensagem:
            print("Não foram encontrados registros para os parâmetros informados.")
        
        # Continua a execução após o print
        print("Elemento encontrado e execução continua...")

    except NoSuchElementException:
        # Trata exceção caso o elemento não seja encontrado
        print("Elemento não encontrado.")

    # try:       
    #     janela = page.find_element(By.XPATH, '//span[@id="component-1003"]')
    #     mensagem = janela.text.strip() 
        
    #     if "Não foram encontrados registros para os parâmetros informados." in mensagem:            
            
    #         print("Não foram encontrados registros para os parâmetros informados..") 
        
        
            
    #         resultado_encontrado = {
    #                 "id": id,
    #                 "filterDate": filterDate,
    #                 "countInvoices_destinadas": '0',
    #                 "countInvoices_emitidas": '0',
    #                 "documentNumber": documentNumber,               
    #                 "description_emitidas": "Cnpj Não Autorizado",
    #                 "description_destinadas": "Cnpj Não Autorizado",
    #                 "escritorioId": escritorioId,
    #                 "empresaId": empresaId,
    #                 "requestNumber_destinadas": "0",
    #                 "requestNumber_emitidas": "0",
    #                 "documentNumberOffice": documentNumberOffice,
    #                 "processDate": "TBD",
    #                 "requestDate": atual_date,
    #                 "referenceDate": formatted_date,
    #                 "razaoSocial": razaoSocial,
    #                 "urlFile_emitidas": 'TBD',
    #                 "urlFile_destinadas": 'TBD',
    #                 "msg": "Agendamento não autorizado para este CNPJ.",
    #                 "status": "FAILED"
    #             }

    #         print("Resultado encontrado:")
    #         print(json.dumps(resultado_encontrado, indent=2, ensure_ascii=False).encode('utf-8').decode('utf-8'))
    #         page.quit()
    #         return resultado_encontrado 
    # except:
    #     pass  
      
    time.sleep(3)    
    page.find_element(By.CSS_SELECTOR, "#ext-gen1030").send_keys(initialDateFormatted)
    page.find_element(By.CSS_SELECTOR, "#ext-gen1030").send_keys(Keys.TAB)
    page.find_element(By.CSS_SELECTOR, "#ext-gen1022").send_keys(initialHour)
    page.find_element(By.CSS_SELECTOR, "#ext-gen1022").send_keys(Keys.TAB)
    page.find_element(By.CSS_SELECTOR, "#ext-gen1032").send_keys(endDateFormatted)
    page.find_element(By.CSS_SELECTOR, "#ext-gen1032").send_keys(Keys.TAB)
    page.find_element(By.CSS_SELECTOR, "#ext-gen1023").send_keys(endHour)
    time.sleep(1)

    page.find_element(By.CSS_SELECTOR, "#ucs20_BtnAgendar").click()
    time.sleep(2)
    
    try:       
        elemento = page.find_element(By.XPATH, '//*[@id="component-1003"]')
        mensagem = elemento.text.strip()     

        if "Este pedido de download DF-e já foi solicitado." in mensagem:
            print("Este pedido de download DF-e já foi solicitado.")
            
            time.sleep(1)
            
            confere = f"{initialDate} 00:00:00 a {endDate} 23:59:59"

            tabela_selector = "table.x-grid-table.x-grid-table-resizer"
            linhas_selector = f"{tabela_selector} tbody tr:not(:first-child)"
            linhas = page.find_elements(By.CSS_SELECTOR, linhas_selector)

            resultados = []

            for linha in linhas:
                celulas_selector = "td"
                celulas = linha.find_elements(By.CSS_SELECTOR, celulas_selector)

                objeto_linha = {}

                time.sleep(1)

                for i, celula in enumerate(celulas, start=1):
                    texto_celula = celula.text.strip()
                    objeto_linha[f"coluna{i}"] = texto_celula

                resultados.append(objeto_linha)

                if objeto_linha["coluna3"] == confere:
                    
                    if objeto_linha["coluna4"] == "Emitidas":
                    
                        status = objeto_linha["coluna10"].split('.')[0].strip()               
                        description_emitidas = status
                        request_number_emitidas = objeto_linha["coluna1"]
                        


                if objeto_linha["coluna3"] == confere:
                    
                    if objeto_linha["coluna4"] == "Destinadas":
                        
                        status = objeto_linha["coluna10"].split('.')[0].strip()              
                        description_destinadas = status
                        request_number_destinadas = objeto_linha["coluna1"] 
                        periodo = objeto_linha["coluna3"]
                        data_processamento = objeto_linha["coluna9"] 
                        data_requisicao = objeto_linha["coluna8"]      
                        
                                    

            resultado_encontrado = {
                "id": id,
                "filterDate": periodo,
                "countInvoices_destinadas": '0',
                "countInvoices_emitidas": '0',
                "documentNumber": documentNumber,               
                "description_emitidas":  description_emitidas,
                "description_destinadas": description_destinadas,
                "escritorioId": escritorioId,
                "empresaId": empresaId,
                "requestNumber_destinadas": request_number_destinadas,
                "requestNumber_emitidas": request_number_emitidas,
                "documentNumberOffice": documentNumberOffice,
                "processDate": data_processamento,
                "requestDate": data_requisicao,
                "referenceDate": formatted_date,
                "razaoSocial": razaoSocial,
                "urlFile_emitidas": 'TBD',
                "urlFile_destinadas": 'TBD',
                "msg": f"Agendamento Cnpj: {documentNumber}, Realizado Com Sucesso",
                "status": "AGENDADO"
            }

            print("Resultado encontrado:")
            print(json.dumps(resultado_encontrado, indent=2, ensure_ascii=False).encode('utf-8').decode('utf-8'))                
            page.quit()
            return resultado_encontrado
            
        
        if "Não foram encontrados registros para os parâmetros informados." in mensagem:
            print("Não foram encontrados registros para os parâmetros informados.")
            page.quit()
            return {"Mensagem": "Este pedido de download DF-e já foi solicitado."}

        if "Agendamento não autorizado para este CNPJ." in mensagem:
            
            print("Agendamento não autorizado para este CNPJ.")                     

            resultado_encontrado = {
                "id": id,
                "filterDate": filterDate,
                "countInvoices_destinadas": '0',
                "countInvoices_emitidas": '0',
                "documentNumber": documentNumber,               
                "description_emitidas": "Cnpj Não Autorizado",
                "description_destinadas": "Cnpj Não Autorizado",
                "escritorioId": escritorioId,
                "empresaId": empresaId,
                "requestNumber_destinadas": "0",
                "requestNumber_emitidas": "0",
                "documentNumberOffice": documentNumberOffice,
                "processDate": "TBD",
                "requestDate": atual_date,
                "referenceDate": formatted_date,
                "razaoSocial": razaoSocial,
                "urlFile_emitidas": 'TBD',
                "urlFile_destinadas": 'TBD',
                "msg": "Agendamento não autorizado para este CNPJ.",
                "status": "FAILED"
            }

            print("Resultado encontrado:")
            print(json.dumps(resultado_encontrado, indent=2, ensure_ascii=False).encode('utf-8').decode('utf-8'))
            page.quit()
            return resultado_encontrado 
       
        if "Existe(m) inconsistência(s) no(s) campo(s) do formulário" in mensagem:
            
            print("Existe(m) inconsistência(s) no(s) campo(s) do formulário")
            
            resultado_encontrado = {
            "id": id,
            "filterDate": filterDate,
            "countInvoices_destinadas": '0',
            "countInvoices_emitidas": '0',
            "documentNumber": documentNumber,               
            "description_emitidas": "Cnpj Não Cadastrado",
            "description_destinadas": "Cnpj Não Cadastrado",
            "escritorioId": escritorioId,
            "empresaId": empresaId,
            "requestNumber_destinadas": "0",
            "requestNumber_emitidas": "0",
            "documentNumberOffice": documentNumberOffice,
            "processDate": "TBD",
            "requestDate": atual_date,
            "referenceDate": formatted_date,
            "razaoSocial": razaoSocial,
            "urlFile_emitidas": 'TBD',
            "urlFile_destinadas": 'TBD',
            "msg": "Cnpj Não Cadastrado",
            "status":"FAILED"
             }
            
            page.find_element(By.XPATH, '//*[@id="button-1009-btnEl"]').click()

            print("Resultado encontrado:")
            print(json.dumps(resultado_encontrado, indent=2, ensure_ascii=False).encode('utf-8').decode('utf-8'))
            page.quit()
            return resultado_encontrado 
            
           
        
        if "Não foram encontrados registros para os parâmetros informados." in mensagem:
            
            print("Não foram encontrados registros para os parâmetros informados")             
            
            resultado_encontrado = {
                "id": id,
                "filterDate": filterDate,
                "countInvoices_destinadas": '0',
                "countInvoices_emitidas": '0',
                "documentNumber": documentNumber,               
                "description_emitidas": "Cnpj Não Cadatrado",
                "description_destinadas": "Cnpj Não Cadatrado",
                "escritorioId": escritorioId,
                "empresaId": empresaId,
                "requestNumber_destinadas": "0",
                "requestNumber_emitidas": "0",
                "documentNumberOffice": documentNumberOffice,
                "processDate": "TBD",
                "requestDate": atual_date,
                "referenceDate": formatted_date,
                "razaoSocial": razaoSocial,
                "urlFile_emitidas": 'TBD',
                "urlFile_destinadas": 'TBD',
                "msg": "Não foram encontrados registros para os parâmetros informados",
                "status": "FAILED"
            }
            page.find_element(By.XPATH, '//*[@id="button-1009-btnEl"]').click()
            print("Resultado encontrado:")
            print(json.dumps(resultado_encontrado, indent=2, ensure_ascii=False).encode('utf-8').decode('utf-8'))
            page.quit()
            return resultado_encontrado 

    except:
        pass
    
    
    page.find_element(By.CSS_SELECTOR, '#ext-gen1116').click()
    time.sleep(1) 
    page.find_element(By.CSS_SELECTOR, "#ucs20_BtnAgendar").click()
    time.sleep(2)  
    
    confere = f"{initialDate} 00:00:00 a {endDate} 23:59:59"

    tabela_selector = "table.x-grid-table.x-grid-table-resizer"
    linhas_selector = f"{tabela_selector} tbody tr:not(:first-child)"
    linhas = page.find_elements(By.CSS_SELECTOR, linhas_selector)

    resultados = []

    for linha in linhas:
        celulas_selector = "td"
        celulas = linha.find_elements(By.CSS_SELECTOR, celulas_selector)

        objeto_linha = {}

        time.sleep(1)

        for i, celula in enumerate(celulas, start=1):
            texto_celula = celula.text.strip()
            objeto_linha[f"coluna{i}"] = texto_celula

        resultados.append(objeto_linha)

        if objeto_linha["coluna3"] == confere:
            
            if objeto_linha["coluna4"] == "Emitidas":
            
                status = objeto_linha["coluna10"].split('.')[0].strip()               
                description_emitidas = status
                request_number_emitidas = objeto_linha["coluna1"]
                


        if objeto_linha["coluna3"] == confere:
            
            if objeto_linha["coluna4"] == "Destinadas":
                
                status = objeto_linha["coluna10"].split('.')[0].strip()              
                description_destinadas = status
                request_number_destinadas = objeto_linha["coluna1"] 
                periodo = objeto_linha["coluna3"]
                data_processamento = objeto_linha["coluna9"] 
                data_requisicao = objeto_linha["coluna8"]      
                
                             

    resultado_encontrado = {
        "id": id,
        "filterDate": periodo,
        "countInvoices_destinadas": '0',
        "countInvoices_emitidas": '0',
        "documentNumber": documentNumber,               
        "description_emitidas":  description_emitidas,
        "description_destinadas": description_destinadas,
        "escritorioId": escritorioId,
        "empresaId": empresaId,
        "requestNumber_destinadas": request_number_destinadas,
        "requestNumber_emitidas": request_number_emitidas,
        "documentNumberOffice": documentNumberOffice,
        "processDate": data_processamento,
        "requestDate": data_requisicao,
        "referenceDate": formatted_date,
        "razaoSocial": razaoSocial,
        "urlFile_emitidas": 'TBD',
        "urlFile_destinadas": 'TBD',
        "msg": f"Agendamento Cnpj: {documentNumber}, Realizado Com Sucesso",
        "status": "AGENDADO"
    }

    print("Resultado encontrado:")
    print(json.dumps(resultado_encontrado, indent=2, ensure_ascii=False).encode('utf-8').decode('utf-8'))                
    page.quit()
    return resultado_encontrado
            
    page.quit()
