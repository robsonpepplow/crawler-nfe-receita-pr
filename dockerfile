# Use uma imagem base do Python
FROM python:3.10-slim

# Instalar dependências do sistema e o Edge
RUN apt-get update && apt-get install -y \
    wget \
    curl \
    gnupg2 \
    libgconf-2-4 \
    libnss3 \
    fonts-liberation \
    libappindicator3-1 \
    xdg-utils \
    xvfb \
    unzip \
    && rm -rf /var/lib/apt/lists/*

# Instalar Microsoft Edge
RUN wget -qO - https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor -o /etc/apt/trusted.gpg.d/microsoft.gpg \
    && echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge.list \
    && apt-get update \
    && apt-get install -y microsoft-edge-stable

# Instalar Microsoft Edge WebDriver (versão específica)
RUN EDGE_DRIVER_VERSION="114.0.1823.43" \
    && wget -O /tmp/edgedriver.zip "https://msedgedriver.azureedge.net/$EDGE_DRIVER_VERSION/edgedriver_linux64.zip" \
    && unzip /tmp/edgedriver.zip -d /usr/local/bin/ \
    && rm /tmp/edgedriver.zip

# Definir diretório de trabalho
WORKDIR /app

# Copiar o código da aplicação
COPY . /app

# Instalar as dependências da aplicação
RUN pip install --no-cache-dir -r requirements.txt

# Expor a porta do Flask
EXPOSE 5000

# Comando para rodar a aplicação Flask
#CMD ["flask", "run"]
CMD ["flask", "run", "--host=0.0.0.0"]

