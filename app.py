from flask import Flask, request, jsonify
from flask_cors import CORS # type: ignore
from nfeDownload import crawlerDownload
from nfeAgendar import crawlerAgendar
from datetime import datetime
import json

app = Flask(__name__)
CORS(app)


@app.route('/api/crawler/nfe/download', methods=['POST'])
def download():
    data = request.json

    id = data['id']
    userNfeReceitaPr = data['userNfeReceitaPr']
    passwordNfeReceitaPr = data['passwordNfeReceitaPr']
    documentNumber = data['documentNumber']
    documentNumberOffice = data['documentNumberOffice']    
    escritorioId = data['escritorioId']
    empresaId = data['empresaId']
    razaoSocial = data['razaoSocial']
    requestNumber_destinadas = data['requestNumber_destinadas']
    requestNumber_emitidas = data['requestNumber_emitidas']
    referenceDate = data['referenceDate']

    dados = crawlerDownload(
        id=id,
        userNfeReceitaPr=userNfeReceitaPr,
        passwordNfeReceitaPr=passwordNfeReceitaPr,
        documentNumber=documentNumber, 
        documentNumberOffice=documentNumberOffice,        
        escritorioId=escritorioId,
        empresaId=empresaId,
        razaoSocial=razaoSocial,
        requestNumber_destinadas=requestNumber_destinadas,
        requestNumber_emitidas=requestNumber_emitidas,
        referenceDate=referenceDate,
    )

    if dados:
        return jsonify(dados)
    else:
        return jsonify({"message": "Nenhum resultado encontrado"})

    
@app.route('/api/crawler/nfe/agendar', methods=['POST'])
def Agendar():  # sourcery skip: inline-variable, remove-unnecessary-else
    data = request.json
    id = data['id']
    userNfeReceitaPr = data['userNfeReceitaPr']
    passwordNfeReceitaPr = data['passwordNfeReceitaPr']
    documentNumber = data['documentNumber']
    documentNumberOffice = data['documentNumberOffice']    
    escritorioId = data['escritorioId']
    empresaId = data['empresaId']
    razaoSocial = data['razaoSocial']
    initialDate = data['initialDate']
    endDate = data['endDate']

    dados = crawlerAgendar(
        id=id,
        userNfeReceitaPr=userNfeReceitaPr,
        passwordNfeReceitaPr=passwordNfeReceitaPr,
        documentNumber=documentNumber,
        documentNumberOffice=documentNumberOffice,        
        escritorioId=escritorioId,
        empresaId=empresaId,
        razaoSocial=razaoSocial,
        initialDate=initialDate,
        endDate=endDate,
    )

    if dados:
        return jsonify(dados)
    else:
        splitDate = initialDate.split('/')
        formattedDate = f"{splitDate[1]}/{splitDate[2]}"
        data_hora_atual = datetime.now()
       
        formato = "%d/%m/%Y %H:%M:%S"
        atualDate = data_hora_atual.strftime(formato)
        filterDate = f"{initialDate} 00:00:00 a {endDate} 23:59:59" 
        
        resultado_encontrado = {
                "id": id,
                "filterDate": filterDate,
                "countInvoices_destinadas": '0',
                "countInvoices_emitidas": '0',
                "documentNumber": documentNumber, 
                "documentNumberOffice": documentNumberOffice,              
                "description_emitidas": "Cnpj Não Autorizado",
                "description_destinadas": "Cnpj Não Autorizado",
                "escritorioId": escritorioId,
                "empresaId": empresaId,
                "requestNumber_destinadas": "0",
                "requestNumber_emitidas": "0",                
                "processDate": "",
                "requestDate": atualDate,
                "referenceDate": formattedDate,
                "razaoSocial": razaoSocial,
                "urlFile_emitidas": 'TBD',
                "urlFile_destinadas": 'TBD',
                "msg": "Agendamento não autorizado para este CNPJ.",
                "status": "COM FALHA"
            }

        return jsonify(resultado_encontrado)


if __name__ == '__main__':
  app.run(debug=True)
